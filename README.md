# 🇺🇸 Ken McKnight - Sr. Solutions Architect

Hi! I'm Ken McKnight, a Sr. Solutions Architect at GitLab working with Enterprise customers. My focus is on helping customers get the most business value out of GitLab across the SDLC. This includes `git` SCM, CI, and security scanning (AI-powered, of course).

> Tip: [Add your own user profile README](https://docs.gitlab.com/ee/user/profile/#add-details-to-your-profile-with-a-readme) 

## :computer: What I Do

* Talk to companies about their challenges and see how GitLab can help
* Demo GitLab
* Execute Proof of Values (aka POC)
* Provide Value Stream Workshops

## :heart: What I Love about GitLab

* [Issues](https://docs.gitlab.com/ee/user/project/issues/) and [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/) (MR) - so smooth to go from idea to coding on a branch with easy team collaboration
* [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk/) - fantastic way to track customer issues
* Our [CI pipelines](https://docs.gitlab.com/ee/ci/pipelines/) + [runners](https://docs.gitlab.com/runner/) - getting automation up and running quickly
* [Security scanners](https://docs.gitlab.com/ee/user/application_security/) - fit right into our CI pipelines
* [GitLab Duo](https://docs.gitlab.com/ee/user/ai_features.html) - code generation and chat

## :newspaper: Favorite GitLab Blogs

* [10 best practices for using AI-powered GitLab Duo Chat](https://about.gitlab.com/blog/2024/04/02/10-best-practices-for-using-ai-powered-gitlab-duo-chat/)
* [Measuring AI effectiveness beyond developer productivity metrics
](https://about.gitlab.com/blog/2024/02/20/measuring-ai-effectiveness-beyond-developer-productivity-metrics/)
* [Learning Python with a little help from AI
](https://about.gitlab.com/blog/2023/11/09/learning-python-with-a-little-help-from-ai-code-suggestions/)

> Tip: See [GitLab Blogs](https://about.gitlab.com/blog/) for a complete list

## :violin: Spare Time

In my spare time I am a violist in several music groups in the San Francisco Bay Area, including [Cambrian Symphony](https://www.cambriansymphony.org/) and [San Jose Lyric Theatre](https://www.lyrictheatre.org/).